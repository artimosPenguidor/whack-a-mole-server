import { ApolloError } from 'apollo-server-express'

import User from './model'

export const createUser = async (uid) => {
  // Create user based on Firebase provisioned UID
  await User.create({
    uid,
    topGameList: [],
    moleSpeed: 10
  })
  return true
}

export const updateMoleSpeed = async (uid, value) => {
  // Verify that the speed is in acceptable range
  if(value < 0 || value > 100) throw new ApolloError('Invalid Value!')

  // Find and update setting in User
  try {
    await User.findOneAndUpdate(
      {uid},
      {
        $set: {
          moleSpeed: value
        }
      }
    )
  } catch (error) {
    throw new ApolloError('There was an issue with your request...')
  }
  return true
}

export const updateTopGameList = async (game) => {
  const { uid } = game
  // Find User
  const foundUser = await User.findOne({uid}).populate('topGameList')
  if(!foundUser) return false
  
  // Take the existing score list and add the new score to it
  const topGameList = [...foundUser.topGameList, game]
    .sort((a, b) => (b.score-a.score)) // sort it by descending order score
    .slice(0, Math.min(5, foundUser.topGameList.length + 1)) // take up to the 5 highest values
    .map(g => g._id) // get only the game object IDs

  // Find and update setting in User
  try {
    await User.findOneAndUpdate(
      {uid},
      {
        $set: {
          topGameList
        }
      }
    )
  } catch (error) {
    throw new ApolloError('There was an issue with your request...')
  }
}