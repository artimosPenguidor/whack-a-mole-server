import mongoose, { Schema } from 'mongoose'

import Game from '../Game/model'

const userSchema = new mongoose.Schema({
  uid: String,
  topGameList: [{ type: Schema.Types.ObjectId, ref: Game }],
  moleSpeed: Number,
})

export default mongoose.model('User', userSchema)