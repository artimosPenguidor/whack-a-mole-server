import mongoose from 'mongoose'

const gameSchema = new mongoose.Schema({
  uid: String,
  time: Number,
  moleSpeed: Number,
  active: Boolean,
  score: Number,
  moles: [Boolean]
})

export default mongoose.model('Game', gameSchema)