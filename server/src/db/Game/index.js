import admin from 'firebase-admin'

import Game from './model'
import User from '../User/model'
import { updateTopGameList } from '../User'

export const startGame = async uid => {
  // Create new Game object with active true and all moles false
  const foundUser = await User.findOne({uid})
  if(!foundUser) return false

  const { moleSpeed } = foundUser

  const newGame = await Game.create({
    uid,
    moleSpeed,
    time: 30,
    active: true,
    score: 0,
    moles: [ false, false, false, false, false ]
  })

  // Call gameLoop with id of newly created game
  gameLoop(newGame._id)
}

export const gameLoop = async id => {
  // Get Game
  const foundGame = await Game.findById(id)
  if(!foundGame) return false

  // If time <= 0 (meaning the game is over), check and update the user
  // if the game's score made it into the top 5 list and then make the game inactive.
  if(foundGame.time <= 0) {
    await updateTopGameList(foundGame)
    await Game.findOneAndUpdate(
      {_id: id},
      {$set: {
        active: false,
        moles: [ false, false, false, false, false ]
      }}
    )
    return
  }

  // Roll each mole to have a chance of changing state, based on
  // the moleSpeed setting that the user can set.
  const moles = [...foundGame.moles].map(m => {
    if(Math.random() > (101 - foundGame.moleSpeed)/100) return !m
    else return m
  })

  // Decrement time by 1
  const time = foundGame.time - 1

  // Find and update Game with new data
  await Game.findOneAndUpdate(
    {_id: id},
    {$set: {
      moles,
      time
    }}
  )

  // Wait 1 second, then call gameLoop(id)
  setTimeout(() => gameLoop(id), 1000)
}

export const whack = async (uid, mole) => {
  // Get Game
  const foundGame = await Game.findOne({uid, active: true})
  if(!foundGame) return false

  // If mole is not active, return
  if(foundGame.moles[mole] !== true) return

  // Increment score and set mole to false 
  const score = foundGame.score + 1
  let moles = [...foundGame.moles]
  moles[mole] = false
  
  // Set another random mole to true
  let newActive
  do {
    newActive = Math.floor(Math.random() * moles.length)
  } while(newActive === mole)

  moles[newActive] = true

  await Game.findOneAndUpdate(
    {_id: foundGame._id},
    {$set: {
      score,
      moles
    }}
  )
}

export const getHighScoreList = async () => {
  let processedUids = []
  let highscores = []

  // Get all games (each user would have up to 5 saved)
  const games = await Game.find()
    .sort('-score') // Sort in descending order by score

  for (let i = 0; i < games.length; i++) {
    const { uid, score } = games[i]
    if(processedUids.includes(uid)) continue
    processedUids.push(uid) // This makes it so you only get the top score of each User

    // Use Firebase to get email from uid
    const firebaseUser = await admin.auth().getUser(uid)
    const email = firebaseUser.toJSON().email
    highscores.push({email, score})
  }

  return highscores
}