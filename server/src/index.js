import dotenv from 'dotenv'
import mongoose from 'mongoose'
import { ApolloServer, AuthenticationError } from 'apollo-server-express'
import admin from 'firebase-admin'
import serviceAccount from '../firebaseServiceAccount.json'

import { typeDefs, resolvers } from './schema'

dotenv.config()

// Firebase Initialize
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: process.env.FIREBASE_DATABASE_URL
})

// MongoDB Connect
mongoose.connect(
  `mongodb://${process.env.MONGO_BASIC_USER}:${process.env.MONGO_BASIC_PASS}@whack-a-mole-db/${process.env.MONGO_DATABASE}`,
  {
    useNewUrlParser: true,
    useFindAndModify: false
  }
)
const db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', () => {
  console.log("Connected to DB!")
})

// Express Server Setup
const app = require('express')()

// GraphQL Server Setup
const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: async ({ req }) => {
    // Gets the auth token (provisioned by Firebase) that's included in every request,
    // verifies it, decodes it, and stores the UID in the context to by used in the
    // queries and mutations
    let user
    if (req.headers.authtoken) {
      try {
        user = await admin.auth().verifyIdToken(req.headers.authtoken)
      } catch (error) {
        throw new AuthenticationError('Unauthorized')
      }
    } else {
      throw new AuthenticationError('Unauthorized')
    }
   
    return { uid: user.uid };
   },
})

server.applyMiddleware({app})

app.listen(process.env.EXPRESS_PORT, () => {
  console.log(`listening on *:${process.env.EXPRESS_PORT}`)
})