import { gql } from 'apollo-server-express'

import User from './db/User/model'
import { createUser, updateMoleSpeed } from './db/User'

import Game from './db/Game/model'
import { startGame, whack, getHighScoreList } from './db/Game'

export const typeDefs = gql`
  type User {
    scoreList: [Int]!
    moleSpeed: Int
  }

  type HighScore {
    email: String!
    score: Int!
  }

  type Game {
    time: Int
    score: Int
    moles: [Boolean]
  }

  type Query {
    user: User
    highScoreList: [HighScore]
    game: Game
  }

  type Mutation {
    createAccount: Boolean
    updateMoleSpeed(value: Int!): Boolean
    startGame: Boolean
    whack(mole: Int!): Boolean
  }
`

export const resolvers = {
  Query: {
    user: async (_parent, _args, { uid }) => {
      // Find and return User from uid
      let foundUser = await User.findOne({uid}).populate('topGameList')
      if(foundUser.topGameList && foundUser.topGameList.length > 0){
        foundUser.scoreList = [...foundUser.topGameList]
          .map(g => g.score)
          .sort((a, b) => (b-a))
      } else {
        foundUser.scoreList = []
      }
      return foundUser
    },
    highScoreList: async () => {
      const highscores = await getHighScoreList()
      return highscores
    },
    game: async (_parent, _args, { uid }) => {
      // Find and return active Game from uid
      const foundGame = await Game.findOne({uid, active: true})
      return foundGame
    },
  },
  Mutation: {
    createAccount: async (_parent, _args, { uid }) => {
      await createUser(uid)
      return true
    },
    updateMoleSpeed: async (_parent, { value }, { uid }) => {
      await updateMoleSpeed(uid, value)
      return true
    },
    startGame: async (_parent, _args, { uid }) => {
      await startGame(uid)
      return true
    },
    whack: async (_parent, { mole }, { uid }) => {
      await whack(uid, mole)
      return true
    },
  },
}
