# /bin/bash
echo "########### $(date) ###############"

cd $1
git fetch --all

current_commit=$(git rev-parse --short HEAD)
remote_commit=$(git rev-parse --short origin/master)
echo $current_commit
echo $remote_commit
if [ "$current_commit" != "$remote_commit" ]; then
  git pull
  message=$(git log -1 --pretty=%B)
  /usr/local/bin/docker-compose -f docker-compose-prod.yml build
  /usr/local/bin/docker-compose -f docker-compose-prod.yml up -d
fi