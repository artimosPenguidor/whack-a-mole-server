# Whack a Mole Server #

This Repository is the backend server for the Whack a Mole game hosted at nicktheprogrammer.com

## Getting Set Up ##

To set up your very own backend server, just follow these steps:

- Make sure you have Docker and Docker-Compose installed on your system
- Copy the `env.template` file and rename it to just `.env`
- Update all the variable values in the `.env` file with your own values
- Follow the instructions on Firebase's website to set up an app and create a service account file named `firebaseServiceAccount.json` and place it in the `server` directory
- Run `docker-compose -f docker-compose-dev.yml up --build`

Your backend server is now available at `localhost:4000`, enjoy!